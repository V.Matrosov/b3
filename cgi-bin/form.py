#!/usr/bin/env python3

import cgi
import html
import sys
import codecs
import datetime
import psycopg2
from psycopg2 import sql


sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())


form = cgi.FieldStorage()
text1 = form.getvalue("name","")
text1 = html.escape(text1)
text2 = form.getvalue("email")
data = datetime.datetime.strptime(form.getvalue("date"), "%Y-%m-%d")


pol = form.getvalue("pol")
status = int(form.getvalue("status"))

if form.getvalue("forma"):
    forma1 = form.getvalue("forma")
else:
    forma1 = "none"

forma = ''.join(forma1)

if form.getvalue('text'):
    bio = form.getvalue('text')
else:
    bio = ""

if form.getvalue("box"):
    box = True
else:
    box = False
    print("Content-type: text/html")
    print("Set-cookie: name=value")
    print()
    print('''<DOCTYPE html>
          <html lang="ru">
          <head>
          <title>backend-3</title>
          <meta charset="UTF-8">
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
          </head>
          <body>
          <h1>Форма заполнена с ошибкой и не отправлена!</h1>
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
          </body>
          </html>
          ''')
    exit()
if text1 =='' or text2 == "" or bio=='':
    print('''<DOCTYPE html>
    <html lang="ru">
    <head>
    <title>backend-3</title>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
    <body>
    <h1>Форма заполнена с ошибкой и не отправлена!</h1>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
    </html>
    ''')
    exit()

conn = psycopg2.connect(
    dbname='postgres',
    user='myuser',
    password='mypassword',
    host='localhost'
)
with conn.cursor() as cursor:
    conn.autocommit = True
    values = [
        (0, text1, text2, data, pol, status, forma1, bio, box),
    ]
    insert = sql.SQL('INSERT INTO form (id_person, first_name, email, nowdate, sex, kolvo, abilities, bio, checkbox) VALUES {}').format(
        sql.SQL(',').join(map(sql.Literal, values))
    )
    cursor.execute(insert)
conn.close()


print("Content-type: text/html")
print("Set-cookie: name=value")
print()
print('''<DOCTYPE html>
    <html lang="ru">
    <head>
    <title>backend-3</title>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
    <body>
    <h1>Форма отправлена!</h1>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
    </html>
    ''')
